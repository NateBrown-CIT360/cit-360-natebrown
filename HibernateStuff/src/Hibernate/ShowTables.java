package Hibernate;

import java.util.List;

public class ShowTables {
    public static void main(String[] args) {



        LibraryDAO l = LibraryDAO.getInstance();

        l.populateBooks();

        List<Library> d = l.getBooks();
        for(Library i : d) {
            System.out.println(i);
        }

    }
}
