package Hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.*;

public class LibraryDAO {

    SessionFactory first = null;
    Session firstsession = null;

    private static LibraryDAO single_instance = null;

    private LibraryDAO() {
        first = HibernateSession.getSessionFactory();
    }

    public static LibraryDAO getInstance() {
        if(single_instance == null){
            single_instance = new LibraryDAO();
        }
        return single_instance;
    }

    public List<Library> getBooks(){
        try{
            firstsession = first.openSession();
            firstsession.getTransaction().begin();
            String sql = "from Hibernate.Library";
            List<Library> libs = (List<Library>) firstsession.createQuery(sql).getResultList();
            firstsession.getTransaction().commit();
            return libs;
        } catch(Exception e) {
            e.printStackTrace();
            firstsession.getTransaction().rollback();
            return null;

        }finally {
            firstsession.close();
        }
    }


    public void populateBooks(){
        try {
            firstsession = first.openSession();
            firstsession.getTransaction().begin();

            Library lib1 = new Library();


            lib1.setAuthor("Danica Novgorodoff");
            lib1.setTitle("Long way down");
            lib1.setLocation("Isle 6");

            Library lib2 = new Library();


            lib2.setAuthor("Justina Ireland");
            lib2.setTitle("Deathless Divide");
            lib2.setLocation("Isle 1");

            Library lib3 = new Library();


            lib3.setAuthor("J.R.R Tolken");
            lib3.setTitle("The Lord of the Rings");
            lib3.setLocation("Isle 9");

            Library lib4 = new Library();


            lib4.setAuthor("Ursula K. Leguin");
            lib4.setTitle("Earthsea");
            lib4.setLocation("Isle 2");

            Library lib5 = new Library();


            lib5.setAuthor("George R. R. Martin");
            lib5.setTitle("A Game of Thrones");
            lib5.setLocation("Isle 1");

            Library lib6 = new Library();


            lib6.setAuthor("Robin Hobb");
            lib6.setTitle("Assassin's Apprentice");
            lib6.setLocation("Isle 1");


            Library lib7 = new Library();


            lib7.setAuthor("Stephen King");
            lib7.setTitle("The Gunslinger");
            lib7.setLocation("Isle 9");

            firstsession.save(lib1);
            firstsession.save(lib2);
            firstsession.save(lib3);
            firstsession.save(lib4);
            firstsession.save(lib5);
            firstsession.save(lib6);
            firstsession.save(lib7);

            firstsession.getTransaction().commit();

        } catch(HibernateException e) {
            e.printStackTrace();
            firstsession.getTransaction().rollback();

        } finally {
            firstsession.close();
        }
    }

}
