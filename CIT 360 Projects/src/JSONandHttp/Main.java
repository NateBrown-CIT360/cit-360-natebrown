package JSONandHttp;

import com.sun.net.httpserver.HttpServer;

import static JSONandHttp.Httpside.httpserver;


public class Main {

    public static void main(String[] args) {
        HttpServer server = httpserver();
        server.start();


        Favorites favorite = JsonConversion.getHttp("http://localhost:8001/testing");
        System.out.println("Favorites for today are: " + favorite.toString());

        //server.stop(0);

    }
}
