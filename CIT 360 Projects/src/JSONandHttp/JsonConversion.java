package JSONandHttp;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class JsonConversion {
    public static Favorites getHttp(String string) {
        //String content = "";
        ObjectMapper mapper = new ObjectMapper();

        Favorites lastagain = null;
        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader read = new BufferedReader(new InputStreamReader(http.getInputStream()));
            //StringBuilder strings = new StringBuilder();
            //String other = "";
            String line = read.readLine();
            //while ((line = read.readLine()) != null) {
            // other.append(line);
            // }

            //content = strings.toString();
            lastagain = mapper.readValue(line, JSON.class);

        } catch (Exception e) {
            System.err.println(e.toString());
        }
        return lastagain;
    }
}
