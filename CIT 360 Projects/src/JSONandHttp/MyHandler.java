package JSONandHttp;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;

public class MyHandler implements HttpHandler {
    //String response;
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {


        OutputStream stream = httpExchange.getResponseBody();
        //StringBuilder htmlbuild = new StringBuilder();
        Favorites favorite1 = new Favorites();
        favorite1.setDessert("Cheesecake");
        favorite1.setHike("RedRock Trail");
        favorite1.setMovie("Iron Man");
        favorite1.setPet("Thor");

       // htmlbuild.append(favorite1.toString());



        //response = htmlbuild.toString();
        String lastly = "";
        lastly = JSON.getJson(favorite1);

        httpExchange.sendResponseHeaders(200,lastly.length());
        stream.write(lastly.getBytes());
        stream.flush();
        stream.close();

    }
}
