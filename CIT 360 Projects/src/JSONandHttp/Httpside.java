package JSONandHttp;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;

public class Httpside {


    public static HttpServer httpserver() {
        HttpServer server = null;
        try {
            server = HttpServer.create(new InetSocketAddress(8001), 0);

            server.createContext("/testing", new MyHandler());
            server.setExecutor(null);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return server;
    }



}
