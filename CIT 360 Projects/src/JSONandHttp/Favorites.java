package JSONandHttp;

public class Favorites {


    /*public Favorites(String pet, String dessert, String movie, String hike){
        this.pet = pet;
        this.dessert = dessert;
        this.movie = movie;
        this.hike = hike;
    }*/
    public String pet;

    public String dessert;

    public String movie;

    public String hike;

    public String getPet() {
        return pet;
    }

    public String getDessert() {
        return dessert;
    }

    public String getMovie() {
        return movie;
    }

    public String getHike() {
        return hike;
    }

    public void setDessert(String dessert) {
        this.dessert = dessert;
    }

    public void setHike(String hike) {
        this.hike = hike;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }

    public void setPet(String pet) {
        this.pet = pet;
    }
}
