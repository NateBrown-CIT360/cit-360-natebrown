package Collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Finale {
    public static void main(String[] args) {


        // Creating objects of addresses that will be used by the Hashmap
        addresses address1 = new addresses();
        address1.setName("John");
        address1.setCity("Los Angeles");
        address1.setAddress("1175 W 2550 E");
        address1.setState("California");

        addresses address2 = new addresses();
        address2.setName("William");
        address2.setCity("Houston");
        address2.setAddress("201 Bakers Street");
        address2.setState("Texas");

        addresses address3 = new addresses();
        address3.setName("Jane");
        address3.setCity("LittleRock");
        address3.setAddress("1129 Unicorn Av");
        address3.setState("Arkansas");

        addresses address4 = new addresses();
        address4.setName("Olivia");
        address4.setCity("Salt Lake City");
        address4.setAddress("405 W 900 E");
        address4.setState("Utah");

        //Creates array lists associated with address information based on the person

        List<String> john = new ArrayList<String>();
        john.add(address1.name);
        john.add(address1.address);
        john.add(address1.City);
        john.add(address1.State);


        List<String> william = new ArrayList<String>();
        william.add(address2.name);
        william.add(address2.address);
        william.add(address2.City);
        william.add(address2.State);

        List<String> jane = new ArrayList<String>();
        jane.add(address3.name);
        jane.add(address3.address);
        jane.add(address3.City);
        jane.add(address3.State);

        List<String> olivia = new ArrayList<String>();
        olivia.add(address4.name);
        olivia.add(address4.address);
        olivia.add(address4.City);
        olivia.add(address4.State);


        //Creates a map where the key is the name of the object and the values will be an array of the Name, Address, City and State.
        System.out.println("___Print the Map___");
        HashMap<String, List<String>> together = new HashMap<String, List<String>>();

        together.put("John",  john);
        together.put("William", william);
        together.put("Jane", jane);
        together.put("Olivia", olivia);

        for (java.util.Map.Entry<String, List<String>> me : together.entrySet()) {
            System.out.print(me.getKey() + ":");
            System.out.println(me.getValue());
        }

            //Uses an if statement to change the olivia list, which then changes the map

        if (together.containsKey("John")) {

            olivia.set(1,address1.address);
            olivia.set(2,address1.City);
            olivia.set(3,address1.State);

        }
        System.out.println("___After Changes to Olivia List____");
        System.out.println(together.get("John"));
        System.out.println(together.get("Olivia"));


       
    }
}
