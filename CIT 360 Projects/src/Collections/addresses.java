package Collections;

public class addresses {
    public String name;
    public String address;
    public String City;
    public String State;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

   public String getAddress() {
        return address;
   }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }
}
