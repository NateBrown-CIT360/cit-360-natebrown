package Handling;

import java.util.Scanner;

public class Math {
    public boolean checker = true;


    static void DivideIntegers(int num1, int num2) throws ArithmeticException{
        if(num1 < 1 || num2 < 1){
            throw new ArithmeticException("Numbers entered must not be zero or less.");
        }
        else {
            System.out.println( num1 / num2);

        }


    }





    public static void main(String[] args) {

        Scanner inputs = new Scanner(System.in);
        boolean checker = true;

        while(checker) {
            try {
                System.out.println("Please enter two numbers above zero that will be divided together.");

                int num1 = inputs.nextInt();

                int num2 = inputs.nextInt();

                System.out.println("The result is ");
                DivideIntegers(num1, num2);
                checker = false;
            } catch (Exception e) {
                System.out.println("Invalid inputs, please try again");
            }
        }


    }
}