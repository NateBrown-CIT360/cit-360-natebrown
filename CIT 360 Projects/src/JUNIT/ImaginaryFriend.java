package JUNIT;

public class ImaginaryFriend {
    public boolean withMe;
    public String name;
    public String lookslike;
    public double howmanylegs;

    public ImaginaryFriend(String name, boolean withMe, String lookslike, double howmanylegs){
        this.name = name;
        this.lookslike = lookslike;
        this.withMe = withMe;
        this.howmanylegs = howmanylegs;
    }

    public boolean isWithMe() {
        return withMe;
    }

    public double getHowmanylegs() {
        return howmanylegs;
    }

    public String getLookslike() {
        return lookslike;
    }

    public String getName() {
        return name;
    }

    public void setHowmanylegs(double howmanylegs) {
        if(howmanylegs > 100){
            throw new IllegalArgumentException("That is entirely too many legs to be an imaginary friend");
        }
        this.howmanylegs = howmanylegs;
    }

    public void setLookslike(String lookslike) {

        this.lookslike = lookslike;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWithMe(boolean withMe) {
        this.withMe = withMe;
    }
}
