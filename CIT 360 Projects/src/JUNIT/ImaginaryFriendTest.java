package JUNIT;

import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

class ImaginaryFriendTest {

    @Test
    void FirstFriend() {
        ImaginaryFriend Fred = new ImaginaryFriend("Fred", true, "Dragon", 4);
        assertTrue(Fred.isWithMe());
        assertEquals("Dragon", Fred.getLookslike());
        assertNotNull(Fred);
        assertEquals("Fred", Fred.getName());
        assertEquals(4, Fred.getHowmanylegs());

    }
    @Test
    void SecondFriend() {
        ImaginaryFriend Sam = new ImaginaryFriend("Sam", true, "Unicorn", 4);
        assertAll("Creates Friend",
                () ->   assertTrue(Sam.isWithMe()),
                () ->   assertEquals("Unicorn", Sam.getLookslike()),
                () ->   assertNotNull(Sam),
                () ->   assertEquals("Sam", Sam.getName()));


    }

    //This one I designed to fail so that I could showcase the timeout exit.
    @Test
    void TimeoutPreemtively(){
        ImaginaryFriend George = new ImaginaryFriend("George", true, "T-rex", 4);
        assertTimeoutPreemptively(Duration.ofSeconds(10), ()-> {
            while(George.isWithMe() == true){
                int crazy = (int) George.howmanylegs++;

            }
        });



    }
    @Test
    void Timeout(){
        ImaginaryFriend Timmy = new ImaginaryFriend("Timmy", true, "Pikachu", 4);
        assertTimeout(Duration.ofSeconds(10), ()-> {
            while(Timmy.isWithMe() == true){
                int crazy = (int) Timmy.howmanylegs + 5;
                Timmy.howmanylegs = crazy;
                break;
            }
        });
    }



    @Test
    void exceptions() {
        ImaginaryFriend Alex = new ImaginaryFriend("Alex", false, "Bear", 4);
        assertThrows(IllegalArgumentException.class,
                () ->   Alex.setHowmanylegs(101)
                );


    }


    /*@Test
    void NotThrow(){
        ImaginaryFriend Sam = new ImaginaryFriend("Sam", false, "Crab", 4);
        assertDoesNotThrow("No exception"){
            Sam.setHowmanylegs(8);
        }
        assertEquals(4, Sam.getHowmanylegs());

    }*/






    }


