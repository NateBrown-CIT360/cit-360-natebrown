package Servlets;

import HibernateSide.Asiainfo;

import HibernateSide.HibernateMethods;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "RedirectAi", urlPatterns={"/RedirectAi"})
public class RedirectAi extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String date = request.getParameter("date");
        String title = request.getParameter("title");
        String description = request.getParameter("entertext");

        try {
            if(title.matches("[<>]+") || description.matches("[<>]+") || date.matches("[<>]+")
                    || title == "" || description == "" || date == ""){
                response.setContentType("text/html");
                out.println("<html><head><link rel=\"stylesheet\" href=\"Style.css\"></head><body>");
                out.println("<h1>Invalid input, please follow the <a href=\"index.jsp\">link.</a></h1>");
                out.println("</body></html>");
            }
            else {
                HibernateMethods l = HibernateMethods.getInstance();
                Asiainfo asiainfo = new Asiainfo();
                asiainfo.setDate(date);
                asiainfo.setTitle(title);
                asiainfo.setDescription(description);

                l.saveAi(asiainfo);


                RequestDispatcher dispatcher = request.getRequestDispatcher("RequestFullfill.jsp");
                dispatcher.forward(request, response);
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");
    }
}
