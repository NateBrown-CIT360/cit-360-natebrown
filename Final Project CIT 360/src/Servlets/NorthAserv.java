package Servlets;




import HibernateSide.HibernateMethods;
import HibernateSide.NoAmericainfo;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


@WebServlet(name = "NorthAserv", urlPatterns={"/NorthAserv"})
public class NorthAserv extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            PrintWriter out = response.getWriter();

            HibernateMethods l = HibernateMethods.getInstance();
            List<NoAmericainfo> d = l.getNoAmerInfo();


            response.setContentType("text/html");
            out.println("<html><head><link rel=\"stylesheet\" href=\"Style.css\"></head>");
            out.println("<body>");
            out.println("<h1>Information related to storms around North America.</h1>");
            for (NoAmericainfo i : d) {
                out.println("</br>");
                out.println("<p>" + i.getDate() + "</p>");
                out.println("<p>" + i.getTitle() + "</p>");
                out.println("<p>" + i.getDescription() + "</p>");
            }
            out.println("</br>");
            out.println("</br>");
            out.println("</br>");
            out.println("</br>");
            out.println("<p> If you would like to submit an event, enter the information below.");
            out.println("<form action= \"RedirectNa\" method=\"Get\">");
            out.println("<p>Date:<input name=\"date\" type=\"text\"/></p>");
            out.println("<p>Title<input name=\"title\" type=\"text\" /></p>");
            out.println("<p><label></label><textarea name=\"entertext\" rows=\"9\" cols=\"80\">Your Text Here</textarea></p>");
            out.println("</br>");
            out.println("<p class = \"para\"><input type=\"submit\" value=\"Submit Form\"/></p>");
            out.println("</form>");
            out.println("</body></html>");
        } catch(Exception e){
            e.printStackTrace();
        }


    }


}