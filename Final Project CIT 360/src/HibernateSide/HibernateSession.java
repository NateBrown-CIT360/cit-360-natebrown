package HibernateSide;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.io.Closeable;

public class HibernateSession {

    private static final SessionFactory sessions = buildSessionFactory();



    private static SessionFactory buildSessionFactory() {
        try{
            StandardServiceRegistry service = new StandardServiceRegistryBuilder().configure("hibernate.config.xml").build();

            Metadata data = new MetadataSources(service).getMetadataBuilder().build();

            return data.getSessionFactoryBuilder().build();
        } catch(Throwable ex) {
            System.err.println("SessionFactory creation failed" + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    public static SessionFactory getSessionFactory(){
        return sessions;
    }
    public static void shutdown() {
        getSessionFactory().close();
    }
}
