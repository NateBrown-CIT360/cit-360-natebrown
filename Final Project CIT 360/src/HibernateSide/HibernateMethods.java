package HibernateSide;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.*;

public class HibernateMethods {
    SessionFactory sessionFactory = null;
    Session session = null;

    private static HibernateMethods single_instance = null;

    private HibernateMethods(){
        sessionFactory = HibernateSession.getSessionFactory();
    }
    public static HibernateMethods getInstance() {
        if(single_instance == null){
            single_instance = new HibernateMethods();
        }
        return single_instance;
    }

    public List<Samericainfo> getSamerInfo() {
        try{

            session = sessionFactory.openSession();
            session.getTransaction().begin();
            List<Samericainfo> Samer = (List<Samericainfo>) session.createQuery("from HibernateSide.Samericainfo").getResultList();
            session.getTransaction().commit();
            return Samer;
        } catch(Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }
    }
    public List<NoAmericainfo> getNoAmerInfo() {
        try{
            session = sessionFactory.openSession();
            session.getTransaction().begin();
            List<NoAmericainfo> Noamer = (List<NoAmericainfo>) session.createQuery("from HibernateSide.NoAmericainfo").getResultList();
            session.getTransaction().commit();
            return Noamer;
        } catch(Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }
    }
    public List<Asiainfo> getAsiaInfo() {
        try{
            session = sessionFactory.openSession();
            session.getTransaction().begin();
            List<Asiainfo> Asia = (List<Asiainfo>) session.createQuery("from HibernateSide.Asiainfo").getResultList();
            session.getTransaction().commit();
            return Asia;
        } catch(Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }
    }
    public List<Australiainfo> getAustInfo() {
        try{

            session = sessionFactory.openSession();
            session.getTransaction().begin();
            List<Australiainfo> Aust = (List<Australiainfo>) session.createQuery("from HibernateSide.Australiainfo").getResultList();
            session.getTransaction().commit();
            return Aust;
        } catch(Exception e) {
            e.printStackTrace();
            System.out.println("Working");
            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }
    }
    public List<Europeinfo> getEruopeInfo() {
        try{
            session = sessionFactory.openSession();
            session.getTransaction().begin();
            List<Europeinfo> Europe = (List<Europeinfo>) session.createQuery("from HibernateSide.Europeinfo").getResultList();
            session.getTransaction().commit();
            return Europe;
        } catch(Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }
    }

    public void saveAu(Australiainfo australiainfo) {
       try {
           session = sessionFactory.openSession();
           session.getTransaction().begin();

           session.save(australiainfo);
           session.getTransaction().commit();

       }catch(Exception e){
           e.printStackTrace();
           session.getTransaction().rollback();
       } finally {
           session.close();
       }
    }
    public void saveAi(Asiainfo asiainfo) {
        try {
            session = sessionFactory.openSession();
            session.getTransaction().begin();

            session.save(asiainfo);
            session.getTransaction().commit();

        }catch(Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }
    public void saveEu(Europeinfo europeinfo) {
        try {
            session = sessionFactory.openSession();
            session.getTransaction().begin();

            session.save(europeinfo);
            session.getTransaction().commit();

        }catch(Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }
    public void saveNa(NoAmericainfo noAmericainfo) {
        try {
            session = sessionFactory.openSession();
            session.getTransaction().begin();

            session.save(noAmericainfo);
            session.getTransaction().commit();

        }catch(Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }
    public void saveSa(Samericainfo samericainfo) {
        try {
            session = sessionFactory.openSession();
            session.getTransaction().begin();

            session.save(samericainfo);
            session.getTransaction().commit();

        }catch(Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }



}

