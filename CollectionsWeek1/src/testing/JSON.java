package testing;

import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JSON {

    public static String customerToJSON(Customer customer) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(customer);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }
    public static Customer JSONToCustomer(String s) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        Customer customer = null;

        try {
            customer = mapper.readValue(s, Customer.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return customer;
    }

    public static void main(String[] args) throws IOException {


        List<Customer> theList = new ArrayList<Customer>();
        Customer cust3 = new Customer();
        cust3.setName("John");
        cust3.setPhone(5595555);

        Customer cust = new Customer();
        cust.setName("Troy");
        cust.setPhone(12345);

        String json = JSON.customerToJSON(cust);
        System.out.println(json);

        Customer cust2 = JSON.JSONToCustomer(json);
        System.out.println(cust2);
        theList.add(cust);
        theList.add(cust2);
        theList.add(cust3);



        for(int i = 0; i<theList.size(); i++) {
            System.out.print(Arrays.asList(theList.get(i)));
        }


    }

}
