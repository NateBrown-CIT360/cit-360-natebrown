package ThreadsRunsExecs;

import java.util.Random;

public class WaldoFinder implements Runnable {

    private String country;
    private int countryID;
    private int searchTime;


    public WaldoFinder(String country, int countryID, int searchTime) {

        this.country = country;
        this.countryID = countryID;
        this.searchTime = searchTime;




    }


    public void run() {
        System.out.println("Now searching for Waldo in " + country + ". " + " ID is " + countryID + "\n\n");

        for(int increment = 1; increment < searchTime; increment++){
            if(increment<searchTime){
                System.out.println("Still searching " + country + " for Waldo.");

                try{
                    Thread.sleep(400);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println("\n" + "Search in " + country + " is complete." );
        System.out.println("\n" + "Days searched: " + searchTime);

    }
}
