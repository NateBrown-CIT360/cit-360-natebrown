package ThreadsRunsExecs;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class FindWaldo {

    public static void main(String[] args) throws InterruptedException {




        ExecutorService searching = Executors.newFixedThreadPool(5);



        WaldoFinder america = new WaldoFinder("America", 1,20);
        WaldoFinder sweden = new WaldoFinder("Sweden", 2,19);
        WaldoFinder germany = new WaldoFinder("Germany", 3,12);
        WaldoFinder russia = new WaldoFinder("Russia", 4,15);
        WaldoFinder egypt = new WaldoFinder("Egypt", 5,18);
        WaldoFinder hawaii = new WaldoFinder("Hawaii", 6,20);
        WaldoFinder quebec = new WaldoFinder("Quebec", 7,14);
        WaldoFinder china = new WaldoFinder("China", 8,11);
        WaldoFinder pakistan = new WaldoFinder("Pakistan", 9,19);
        WaldoFinder poland = new WaldoFinder("Poland", 10,20);

        searching.execute(america);
        searching.execute(sweden);
        searching.execute(germany);
        searching.execute(russia);
        searching.execute(egypt);
        searching.execute(hawaii);
        searching.execute(quebec);
        searching.execute(china);
        searching.execute(pakistan);
        searching.execute(poland);



        searching.shutdown();
        searching.awaitTermination(20, TimeUnit.SECONDS);

        int waldo;
            Random num = new Random();
            waldo = num.nextInt(10);

            switch (waldo) {
                case 1:
                    System.out.println("Waldo was found in America!!");
                    break;
                case 2:
                    System.out.println("Waldo was found in Sweden!!");
                    break;
                case 3:
                    System.out.println("Waldo was found in Germany!!");
                    break;
                case 4:
                    System.out.println("Waldo was found in Russia!!");
                    break;
                case 5:
                    System.out.println("Waldo was found in Egypt!!");
                    break;
                case 6:
                    System.out.println("Waldo was found in Hawaii!!");
                    break;
                case 7:
                    System.out.println("Waldo was found in Quebec!!");
                    break;
                case 8:
                    System.out.println("Waldo was found in China!!");
                    break;
                case 9:
                    System.out.println("Waldo was found in Pakistan!!");
                    break;
                case 10:
                    System.out.println("Waldo was found in Poland!!");
                    break;



        }

    }
}
