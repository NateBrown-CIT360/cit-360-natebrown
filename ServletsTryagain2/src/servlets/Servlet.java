package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/** This is a simple servlet that implements doPost and doGet.  The doPost takes
 *  two parameters (login and password) and displays them.
 */
@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {

    /** this is the main method that uses the two parameters and displays them. */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head>");
        out.println("<body bgcolor = \"#EE9582\">");
        String first = request.getParameter("firstName");
        String last = request.getParameter("lastName");
        out.println("<h1 align = \"center\">Form Information</h1>");
        out.println("<p>First Name: " + first + "</p>");
        out.println("<p>Last Name: " + last + "</p>");
        out.println("<ul>\n<li><b> Happy : </b>: " + request.getParameter("happy"));
        out.println("<li><b> Sad : </b>: " + request.getParameter("sad"));
        out.println("<li><b> Jealous : </b>: " + request.getParameter("jealous"));
        out.println("<li><b> Anxious : </b>: " + request.getParameter("anxious"));
        out.println("<p>City: " + request.getParameter("city"));
        out.println("<p>State: " + request.getParameter("state"));
        out.println("<p>Weather: " + request.getParameter("weather"));

        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");
    }

}
